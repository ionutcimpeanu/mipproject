package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the medicalstaff database table.
 * 
 */
@Entity
@NamedQuery(name="Medicalstaff.findAll", query="SELECT m FROM Medicalstaff m")
public class Medicalstaff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idMedicalStaff;

	private String medicName;

	public Medicalstaff() {
	}
	public Medicalstaff(String medicName) {
		this.medicName=medicName;
	}
	public Medicalstaff(int idMedicalStaff) {
		this.idMedicalStaff=idMedicalStaff;
	}
	public int getIdMedicalStaff() {
		return this.idMedicalStaff;
	}

	public void setIdMedicalStaff(int idMedicalStaff) {
		this.idMedicalStaff = idMedicalStaff;
	}

	public String getMedicName() {
		return this.medicName;
	}

	public void setMedicName(String medicName) {
		this.medicName = medicName;
	}

}