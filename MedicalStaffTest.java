package unitTests;

import org.junit.Assert;
import org.junit.Test;

import model.Medicalstaff;

public class MedicalStaffTest {

	@Test
	public void ShouldCreateMedicalStaffAndReturnName() {
		String medicName = "Smith";
		
		Medicalstaff medicalStaff = new Medicalstaff(medicName);
		
		Assert.assertTrue(medicalStaff.getMedicName().equals(medicName));
		
		
	}
	
	@Test
	public void ShouldCreateMedicalStaffID() {
		int medicId = 0010;
		
		Medicalstaff medicalStaff = new Medicalstaff(medicId);
			medicalStaff.setIdMedicalStaff(medicId);
		Assert.assertTrue(medicalStaff.getIdMedicalStaff()==medicId);
		
		
	}
}
