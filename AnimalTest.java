package unitTests;



import org.junit.Assert;
import org.junit.Test;


import model.Animal;

public class AnimalTest {
	
	@Test
	public void ShouldCreateAnimalAndReturnName() {
		String name = "Aila";
		Animal animal = new Animal(name);
		
		Assert.assertTrue(animal.getName().equals(name));
	}
	
	@Test
	public void ShouldBeAbleToGetAndSetAnimalsAge() {
		int age = 5;
		Animal animal2 = new Animal("Pustiu");
		animal2.setAge(age);
		Assert.assertTrue(animal2.getAge() == (age));
		
	}
	
	@Test
	public void ShouldBeAbleToGetAndSetAnimalId() {
		
		int animalId = 0001;
		Animal animal3 = new Animal(animalId);
		animal3.setIdAnimal(animalId);
		
		Assert.assertTrue(animal3.getIdAnimal()==(animalId));
	}

}
