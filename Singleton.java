package singleton;

public class Singleton {
	private static Singleton single_instance = null;

	public String s;

	// Constructor privat destinat doar acestei clase
	private Singleton() {
		s = "Hello I am a string part of Singleton class";
	}
	//Ich denke es ist eine faule Initialisierung
	// metoda static pentru generarea clasei singleton
	public static Singleton getInstance() {
		if (single_instance == null)
			single_instance = new Singleton();

		return single_instance;
	}
}
