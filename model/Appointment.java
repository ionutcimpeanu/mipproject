package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the appointments database table.
 * 
 */
@Entity
@Table(name="appointments")
@NamedQuery(name="Appointment.findAll", query="SELECT a FROM Appointment a")
public class Appointment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idAppointments;

	private String animal;

	private String appointmentDate;

	private String lasting;

	private String medic;
	
	
	
	public Appointment() {
	
	}


	public Appointment(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}
	
	
	public Appointment(int idAppointments) {
		this.idAppointments=idAppointments;
	}



	public int getIdAppointments() {
		return this.idAppointments;
	}

	public void setIdAppointments(int idAppointments) {
		this.idAppointments = idAppointments;
	}

	public String getAnimal() {
		return this.animal;
	}

	public void setAnimal(String animal) {
		this.animal = animal;
	}

	public String getAppointmentDate() {
		return this.appointmentDate;
	}

	public void setAppointmentDate(String appointmentDate) {
		this.appointmentDate = appointmentDate;
	}

	public String getLasting() {
		return this.lasting;
	}

	public void setLasting(String lasting) {
		this.lasting = lasting;
	}

	public String getMedic() {
		return this.medic;
	}

	public void setMedic(String medic) {
		this.medic = medic;
	}

}