package util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Appointment;
import model.Medicalstaff;

public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;

	public void setUP()  {
		entityManagerFactory = Persistence.createEntityManagerFactory("VetShopJPA");
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}

	public void savePersonal(Medicalstaff pers) {
		entityManager.persist(pers);
	}

	public void saveAppointment(Appointment prog) {
		entityManager.persist(prog);
	}

	public void startTransaction() {
		entityManager.getTransaction().begin();
	}

	public void commitTransaction() {
		entityManager.getTransaction().commit();
	}

	public void closeEntityManager() {
		entityManager.close();
	}

	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from vetshopjpa.animal", Animal.class)
				.getResultList();
		for (Animal animal : results) {
			System.out.println("Animal ID is: " + animal.getIdAnimal() + ", name: " + animal.getName());
		}
	}

	
	public void printAllDoctorsFromDB() {
		List<Medicalstaff> results = entityManager.createNativeQuery("Select * from vetshopjpa.medicalstaff", Medicalstaff.class)
				.getResultList();
		for (Medicalstaff pers : results) {
			System.out.println("Medicalpersonnel ID is: " + pers.getIdMedicalStaff() + ", name: " + pers.getMedicName());
		}
	}

	public void printAllAppointmentsFromDB() {
		List<Appointment> results = entityManager.createNativeQuery("Select * from vetshopjpa.appointments", Appointment.class)
				.getResultList();
		for (Appointment appo : results) {
			System.out.println("Appointment ID is: " + appo.getIdAppointments() + ", animal: " + appo.getAnimal() + " medic: "
					+ appo.getMedic() + " date: " + appo.getAppointmentDate() + ", the timed required: " + appo.getLasting());
		}
	}

	public final Animal animalFind(int index) {
		List<Animal> results = entityManager
				.createNativeQuery("Select * from vetshopjpa.animal where idanimal = " + index, Animal.class) 
				.getResultList();
		return results.get(0);
	}

	public void updateAnimal(int index, String newName) {
		Animal animal = (Animal) entityManager.find(Animal.class, index);
		animal.setName(newName);
	}

	public void deleteAnimal(int index) {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from vetshopjpa.appointments where idAppointments = " + index, Appointment.class)
				.getResultList();
		for (Appointment appo : results) {
			entityManager.remove(appo);
		}
		Animal animal = (Animal) entityManager.find(Animal.class, index);
		entityManager.remove(animal);
	}

	public final Medicalstaff doctorFind(int index) {
		List<Medicalstaff> results = entityManager
				.createNativeQuery("Select * from vetshopjpa.medicalstaff where idMedicalStaff =" + index, Medicalstaff.class)
				.getResultList();
		return results.get(0);
	}

	public void updateDoctor(int index, String newName) {
		Medicalstaff pers = (Medicalstaff) entityManager.find(Medicalstaff.class, index);
		pers.setMedicName(newName);
	}

	public void deleteDoctor(int index) {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from vetshopjpa.appointments where idMedicalStaff = " + index, Appointment.class)
				.getResultList();
		for (Appointment app : results) {
			entityManager.remove(app);
		}
		Medicalstaff pers = (Medicalstaff) entityManager.find(Medicalstaff.class, index);
		entityManager.remove(pers);
		// entityManager.getTransaction().commit();
	}

	public final Appointment appointmentFind(int index) {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from petshop.programare where idprogramare =" + index, Appointment.class)
				.getResultList();
		return results.get(0);
	}

	public void deleteAppointment(int index) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		entityManager.remove(appo);
	}

	public void updateAppointmentID(int index, int ID) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		appo.setIdAppointments(ID);
	}

	public void updateAppointmentDoctor(int index, String med) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		appo.setMedic(med);
	}

	public void updateAppointmentDate(int index, String dataa) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		appo.setAppointmentDate(dataa);
	}
	
	public void updateAppointmentAnimal(int index, String an) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		appo.setAnimal(an);
	}
	
	public void updateAppointmentLasting(int index, String last) {
		Appointment appo = (Appointment) entityManager.find(Appointment.class, index);
		appo.setLasting(last);
	}

	public void sortPrint() {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from vetshopjpa.appointments order by AppointmentDate", Appointment.class).getResultList();
		for (Appointment appo : results) {
			System.out.println("Appointments id: " + appo.getIdAppointments() + ", animal: "
					+ appo.getAnimal() + ", personal med: "
					+ appo.getMedic());
		}
	}
	
	public List<Animal>animalList(){
		List<Animal> animalList=(List<Animal>)entityManager.createQuery("Select * a from Animal a",Animal.class).getResultList();
		return animalList;
	}

	public void stop() {
		// TODO Auto-generated method stub
		
	}


}