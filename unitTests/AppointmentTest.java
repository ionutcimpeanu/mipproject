package unitTests;

import org.junit.Assert;
import org.junit.Test;

import model.Appointment;

public class AppointmentTest {
	
	@Test
	public void ShouldCreateAppointmentDate() {
		String appointmentDate = "12.02.2019";
		Appointment appointment = new Appointment (appointmentDate);
	
		Assert.assertTrue(appointment.getAppointmentDate().equals(appointmentDate));
	}
	
	@Test
	public void ShouldCreateAndSetAppointmentId() {
		int appointmentId=0001;
		
		Appointment appointment2 = new Appointment(appointmentId);
		appointment2.setIdAppointments(appointmentId);
		
		Assert.assertTrue(appointment2.getIdAppointments()==appointmentId);
	}
	
	@Test
	public void ShouldCreateAndSetMedic() {
		String medic = "Smith";
		
		Appointment appointment3 = new Appointment(medic);
		appointment3.setMedic(medic);
		
		Assert.assertTrue(appointment3.getMedic()==medic);
	}

	@Test
	public void ShouldCreateAndSetAnimal() {
		String animal = "Aila";
		
		Appointment appointment4 = new Appointment(animal);
		appointment4.setAnimal(animal);
		
		Assert.assertTrue(appointment4.getAnimal()==animal);
	}
	
	@Test
	public void ShouldCreateAndSetLasting() {
		String lasting = "30";
		
		Appointment appointment4 = new Appointment(lasting);
		appointment4.setLasting(lasting);
		
		Assert.assertTrue(appointment4.getLasting()==lasting);
	}
}
