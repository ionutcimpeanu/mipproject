package main;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import model.Animal;
import model.Medicalstaff;
import singleton.Singleton;
import model.Appointment;
import util.DatabaseUtil;
import walk.Location;

public class Main {

	private static Map<Integer, Location> locations = new HashMap<>();

	
	public static void main(String[] args) throws Exception {
		@SuppressWarnings("Statanic Army")
		Scanner keyboard = new Scanner(System.in);
		while (true) {
			DatabaseUtil dbUtil = new DatabaseUtil();
			dbUtil.setUP();
			dbUtil.startTransaction();
			System.out.println(
					"Press 1 for Create,\n2 for Read,\n3 for Update,\n4 for delete,\n5 for sorted print,\n0 for exit: ");

			int choice = keyboard.nextInt();

			if (choice == 0) {
				break;
			}
			if (choice == 1) {
				System.out.println(
						"Press 1 for create a record in Animal,\n2 for Doctor,\n3 for Appointment,\n0 exit");
				System.out.print("Your choice ");
				int choice2 = keyboard.nextInt();
				if (choice2 == 1) {
					System.out.print("Animal ID: ");
					int id = keyboard.nextInt();
					System.out.println("Animal Name: ");
					String name = keyboard.next();
					Animal temp = new Animal();
					temp.setIdAnimal(id);
					temp.setName(name);
					dbUtil.saveAnimal(temp);
					dbUtil.commitTransaction();
				}

				Animal animal = new Animal();
				animal.setIdAnimal(1);
				animal.setName("Tom");
				animal.setAge(2);
			
				
				if (choice2 == 2) {
					System.out.print("Medic ID: ");
					int id = keyboard.nextInt();
					System.out.println("Medic Name: ");
					String name = keyboard.next();
					Medicalstaff temp = new Medicalstaff();
					temp.setIdMedicalStaff(id);
					temp.setMedicName(name);
					dbUtil.savePersonal(temp);
					dbUtil.commitTransaction();
				}

				if (choice2 == 3) {
					System.out.print("Appointment ID: ");
					int id = keyboard.nextInt();
					System.out.print("Appointment date: ");
					String date = keyboard.next();
					System.out.print("Medic: ");
					String med = keyboard.next();
					System.out.println("Animal: ");
					String anima = keyboard.next();
					System.out.println("duration: ");
					String dura = keyboard.next();
					
					Appointment temp = new Appointment();
					temp.setIdAppointments(id);
					temp.setAppointmentDate(date);
					temp.setMedic(med);
					temp.setAnimal(anima);
					temp.setLasting(dura);
					dbUtil.saveAppointment(temp);
					dbUtil.commitTransaction();
				}
				
			}
			if (choice == 2) {
				System.out.println("Press 1 for reading an Animal,\n2 for Medic,\n3 for Appointment,\n0 exit");
				System.out.print("Your choice is: ");
				int ch2 = keyboard.nextInt();
				if (ch2 == 1) {
					dbUtil.printAllAnimalsFromDB();
				}
				if (ch2 == 2) {
					dbUtil.printAllDoctorsFromDB();
				}
				if (ch2 == 3) {
					dbUtil.printAllAppointmentsFromDB();
				}
				dbUtil.commitTransaction();
			}
			
			if (choice == 3) {
				System.out.println("Press 1 for update Animal,\n2 for Doctor,\n3 for Appointment,\n0exit");
				System.out.print("Your choix: ");
				int cho2 = keyboard.nextInt();
				System.out.print("Enter the ID: ");
				int id = keyboard.nextInt();
				if (cho2 == 1) {
					System.out.println("New name: ");
					String name = keyboard.next();
					dbUtil.updateAnimal(id, name);
				}
				if (cho2 == 2) {
					System.out.println("New name: ");
					String name = keyboard.next();
					dbUtil.updateDoctor(id, name);
				}
				if (cho2 == 3) {
					System.out.println("Press 1 for new update ID,\n2 for new medic,\n3 for new Date,\n4 for new animal\n5 for new duration0exit");
					System.out.print("Your option ");
					int cho3 = keyboard.nextInt();
					
					if (cho3 == 1) {
						System.out.println("Enter new appointment ID: ");
						int temp = keyboard.nextInt();
						dbUtil.updateAppointmentID(id, temp);
					}
					
					if (cho3 == 2) {
						System.out.println("Enter new medic: ");
						String temp = keyboard.next();
						dbUtil.updateAppointmentDoctor(id, temp);
					}
					
					if (cho3 == 3) {
						System.out.println("Enter new appointment date: ");
						String timee = keyboard.next();
						dbUtil.updateAppointmentDate(id, timee);
					}
					
					if (cho3 == 4) {
						System.out.println("Enter new animal: ");
						String anim = keyboard.next();
						dbUtil.updateAppointmentAnimal(id, anim);
					}
					
					if (cho3 == 5) {
						System.out.println("Enter new appointment duration: ");
						String dur = keyboard.next();
						dbUtil.updateAppointmentLasting(id, dur);
					}
					
				}
				dbUtil.commitTransaction();
			}
			
			if (choice == 4) {
				System.out.println("Press 1 to delete an Animal,\n2 a medic,\n3 an Appointment,\n0exit");
				System.out.print("Your choice is: ");
				int choice2 = keyboard.nextInt();
				System.out.print("\nwith the ID: ");
				int id = keyboard.nextInt();
				
				if (choice2 == 1) {
					dbUtil.deleteAnimal(id);
				}
				if (choice2 == 2) {
					dbUtil.deleteDoctor(id);
				}
				if (choice2 == 3) {
					dbUtil.deleteAppointment(id);
				}
				dbUtil.commitTransaction();
			}

			if (choice == 5) {
				dbUtil.sortPrint();
			}
			dbUtil.closeEntityManager();
		}
		
		/**
		 * @author John OnCompleteListener basic example on my DB
		 */
		DatabaseUtil dbUtil = new DatabaseUtil();
		LongRunningTask LongRunningTask = new LongRunningTask();
		LongRunningTask.setOnCompleteListener(new OnCompleteListener() {
			@Override
			public void onComplete() {
				System.out.println("Printing all Appointments from my DB: \n");
				dbUtil.printAllAppointmentsFromDB();
				System.out.println("\nThe long running task has been completed!");
			}
		});
		System.out.println("\nA long running task has been started...");
		LongRunningTask.run();


		
		
		/**
		 * @author John Called basic method to sort animal objects by age ascending
		 */
		List<Animal> animal = new ArrayList<>();
		animal.add("Tom");
		animal.add("Bob");
		
		Collections.sort(animal, (e1, e2) -> e1.compareTo(e2));

		for (Animal a : animal) {
			System.out.println(a.getName());
		}

		/**
		 * @author John Basic example for a Singleton that's not related to the main
		 *         purpose of this 
		 */
		Singleton x = Singleton.getInstance();

		Singleton y = Singleton.getInstance();

		Singleton z = Singleton.getInstance();

		x.s = (x.s).toUpperCase();

		System.out.println("String from x is " + x.s);
		System.out.println("String from y is " + y.s);
		System.out.println("String from z is " + z.s);

		z.s = (z.s).toLowerCase();

		System.out.println("String from x is " + x.s);
		System.out.println("String from y is " + y.s);
		System.out.println("String from z is " + z.s);
//HashMap
		Scanner scanner = new Scanner(System.in);
		
		locations.put(0, new Location(0, "You are home with your pet " + ((Animal) animal).getName()));
		locations.put(1,
				new Location(1, "You are home and your pet " + ((Animal) animal).getName() + " wants to go out on a walk."));
		locations.put(2, new Location(2, "You are in the park with your pet " + ((Animal) animal).getName() + "."));
		locations.put(3, new Location(3, "You are at a small lake where he likes to watch the fishes swim"));
		locations.put(4, new Location(4, "You are with some other doggos having fun"));
		locations.put(5,
				new Location(5, "You are on a bench waiting for " + ((Animal) animal).getName() + " to finish his treat."));

		locations.get(1).addExit("W", 2);
		locations.get(1).addExit("E", 3);
		locations.get(1).addExit("S", 4);
		locations.get(1).addExit("N", 5);

		locations.get(2).addExit("N", 5);

		locations.get(3).addExit("W", 1);

		locations.get(4).addExit("N", 1);
		locations.get(4).addExit("w", 2);

		locations.get(5).addExit("S", 1);
		locations.get(5).addExit("W", 2);

		Map<String, String> vocabulary = new HashMap<>();
		vocabulary.put("QUIT", "Q");

		vocabulary.put("NORTH", "N");
		vocabulary.put("SOUTH", "S");
		vocabulary.put("WEST", "W");
		vocabulary.put("EAST", "E");

		int loc = 1;
		while (true) {
			System.out.println(locations.get(loc).getDescription());
			if (loc == 0) {
				break;
			}
			Map<String, Integer> exits = locations.get(loc).getExits();
			System.out.println("Available exits are: ");
			for (String exit : exits.keySet()) {
				System.out.println(exit + ", ");
			}
			System.out.println();
			String direction = scanner.nextLine().toUpperCase();
			if (exits.containsKey(direction)) {
				loc = exits.get(direction);
			} else {
				System.out.println("You cannot go in that direction!");
			}
		}
	}
	

}
System.out.println("\nEND OF PROGRAM\n");
		
	}

}