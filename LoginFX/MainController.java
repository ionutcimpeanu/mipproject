package LoginFX;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class MainController {
	
	@FXML 
	private Label lblStatus;
	
	@FXML 
	private TextField txtUserName;
	
	@FXML 
	private TextField txPassword;

	public void Login(ActionEvent event) throws Exception{
		if(txtUserName.getText().equals("SatanicArmy") && txPassword.getText().equals("1q2w3e")) {
			lblStatus.setText("Loggin Succes!");
			Stage primaryStage=new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("LoginFX/Login.fxml"));
			Scene scene =new Scene(root,400,400);
			scene.getStylesheets().add(getClass().getResource("LoginFX.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.show();
		}else {
			lblStatus.setText("Login Failed!");
		}
	}
}
