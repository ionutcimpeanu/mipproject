package ChangeListener;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class PaintPanel {

	private int x,y;
	private OptionalWindow optionsWindow;
	
	public PaintPanel() {
		optionsWindow = new OptionalWindow();
		theListener = new Listener();
		this.addMouseListener(theListener);
		repaint();
	}
	
	public void paintComponent(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, PaintWindow.WIDTH, PaintWindow.HEIGHT);
		g.setColor(Color.black);
		g.fillOval(x, y,50 , 50);
	}
	
	
	private class Listener implements MouseListener{
		
		public void mouseClicked(MouseEvent e) {
			
		}
		
		public void MousePressed(MouseEvent e) {
			x=e.getX();
			y=e.getY();
			repaint();
		}
		public void mouseReleased(MouseEvent e) {
			
		}
		
		public void mouseEntered(MouseEvent e) {
			
		}
		
		public void mouseExited(MouseEvent e) {
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
} 

