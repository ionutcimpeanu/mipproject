package main;


import java.util.Scanner;

import model.Animal;
import model.Medicalstaff;
import model.Appointment;
import util.DatabaseUtil;

public class Main {

	public static void main(String[] args) throws Exception {
		@SuppressWarnings("Statanic Army")
		Scanner keyboard = new Scanner(System.in);
		while (true) {
			DatabaseUtil dbUtil = new DatabaseUtil();
			dbUtil.setUP();
			dbUtil.startTransaction();
			System.out.println(
					"Press 1 for Create,\n2 for Read,\n3 for Update,\n4 for delete,\n5 for sorted print,\n0 for exit: ");

			int choice = keyboard.nextInt();

			if (choice == 0) {
				break;
			}
			if (choice == 1) {
				System.out.println(
						"Press 1 for create a record in Animal,\n2 for Doctor,\n3 for Appointment,\n0 exit");
				System.out.print("Your choice ");
				int choice2 = keyboard.nextInt();
				if (choice2 == 1) {
					System.out.print("Animal ID: ");
					int id = keyboard.nextInt();
					System.out.println("Animal Name: ");
					String name = keyboard.next();
					Animal temp = new Animal();
					temp.setIdAnimal(id);
					temp.setName(name);
					dbUtil.saveAnimal(temp);
					dbUtil.commitTransaction();
				}

				if (choice2 == 2) {
					System.out.print("Medic ID: ");
					int id = keyboard.nextInt();
					System.out.println("Medic Name: ");
					String name = keyboard.next();
					Medicalstaff temp = new Medicalstaff();
					temp.setIdMedicalStaff(id);
					temp.setMedicName(name);
					dbUtil.savePersonal(temp);
					dbUtil.commitTransaction();
				}

				if (choice2 == 3) {
					System.out.print("Appointment ID: ");
					int id = keyboard.nextInt();
					System.out.print("Appointment date: ");
					String date = keyboard.next();
					System.out.print("Medic: ");
					String med = keyboard.next();
					System.out.println("Animal: ");
					String anima = keyboard.next();
					System.out.println("duration: ");
					String dura = keyboard.next();
					
					Appointment temp = new Appointment();
					temp.setIdAppointments(id);
					temp.setAppointmentDate(date);
					temp.setMedic(med);
					temp.setAnimal(anima);
					temp.setLasting(dura);
					dbUtil.saveAppointment(temp);
					dbUtil.commitTransaction();
				}
				
			}
			if (choice == 2) {
				System.out.println("Press 1 for reading an Animal,\n2 for Medic,\n3 for Appointment,\n0 exit");
				System.out.print("Your choice is: ");
				int ch2 = keyboard.nextInt();
				if (ch2 == 1) {
					dbUtil.printAllAnimalsFromDB();
				}
				if (ch2 == 2) {
					dbUtil.printAllDoctorsFromDB();
				}
				if (ch2 == 3) {
					dbUtil.printAllAppointmentsFromDB();
				}
				dbUtil.commitTransaction();
			}
			
			if (choice == 3) {
				System.out.println("Press 1 for update Animal,\n2 for Doctor,\n3 for Appointment,\n0exit");
				System.out.print("Your choix: ");
				int cho2 = keyboard.nextInt();
				System.out.print("Enter the ID: ");
				int id = keyboard.nextInt();
				if (cho2 == 1) {
					System.out.println("New name: ");
					String name = keyboard.next();
					dbUtil.updateAnimal(id, name);
				}
				if (cho2 == 2) {
					System.out.println("New name: ");
					String name = keyboard.next();
					dbUtil.updateDoctor(id, name);
				}
				if (cho2 == 3) {
					System.out.println("Press 1 for new update ID,\n2 for new medic,\n3 for new Date,\n4 for new animal\n5 for new duration0exit");
					System.out.print("Your option ");
					int cho3 = keyboard.nextInt();
					
					if (cho3 == 1) {
						System.out.println("Enter new appointment ID: ");
						int temp = keyboard.nextInt();
						dbUtil.updateAppointmentID(id, temp);
					}
					
					if (cho3 == 2) {
						System.out.println("Enter new medic: ");
						String temp = keyboard.next();
						dbUtil.updateAppointmentDoctor(id, temp);
					}
					
					if (cho3 == 3) {
						System.out.println("Enter new appointment date: ");
						String timee = keyboard.next();
						dbUtil.updateAppointmentDate(id, timee);
					}
					
					if (cho3 == 4) {
						System.out.println("Enter new animal: ");
						String anim = keyboard.next();
						dbUtil.updateAppointmentAnimal(id, anim);
					}
					
					if (cho3 == 5) {
						System.out.println("Enter new appointment duration: ");
						String dur = keyboard.next();
						dbUtil.updateAppointmentLasting(id, dur);
					}
					
				}
				dbUtil.commitTransaction();
			}
			
			if (choice == 4) {
				System.out.println("Press 1 to delete an Animal,\n2 a medic,\n3 an Appointment,\n0exit");
				System.out.print("Your choice is: ");
				int choice2 = keyboard.nextInt();
				System.out.print("\nwith the ID: ");
				int id = keyboard.nextInt();
				
				if (choice2 == 1) {
					dbUtil.deleteAnimal(id);
				}
				if (choice2 == 2) {
					dbUtil.deleteDoctor(id);
				}
				if (choice2 == 3) {
					dbUtil.deleteAppointment(id);
				}
				dbUtil.commitTransaction();
			}

			if (choice == 5) {
				dbUtil.sortPrint();
			}
			dbUtil.closeEntityManager();
		}

		System.out.println("\nEND OF PROGRAM\n");
	}

}