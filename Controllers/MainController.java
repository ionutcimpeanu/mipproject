package Controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import model.Animal;
import util.DatabaseUtil;

public class MainController implements Initializable {

	
	
	@FXML
	private ListView<String> listView;
	

	public void populateMainListView() {
		DatabaseUtil db= new DatabaseUtil();
		db.setUP();
		db.startTransaction();
		List<Animal> animalDBList=(List<Animal>) db.animalList();
		ObservableList<String> animalNameList = getAnimalName(animalDBList);
		listView.setItems(animalNameList);
		listView.refresh();
		db.stop();
		
	}
	
	
	public ObservableList<String> getAnimalName (List<Animal> animals){
		ObservableList<String> names=FXCollections.observableArrayList();
		for(Animal a: animals) {
			names.add(a.getName());
		}
		return names;
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		populateMainListView();
		
	}

}
